package com.spring.phonebookbackend.controller;

import com.spring.phonebookbackend.entity.Contact;
import com.spring.phonebookbackend.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;


@RestController
@RequestMapping(value = "/phone-book")
public class ContactController {

    @Autowired
    private ContactService contactService;


    @RequestMapping(method = POST,value = "/addContact")
    public Contact addProduct(@RequestBody Contact contact) {
        return contactService.saveContact(contact);
    }

    @RequestMapping(method = POST,value = "/addContacts")
    public List<Contact> addProducts(@RequestBody List<Contact> contacts) {
        return contactService.saveContacts(contacts);
    }

    @RequestMapping(method = GET,value = "/getAllContacts")
    public List<Contact> findAllProducts() {
        return contactService.getContacts();
    }

    @RequestMapping(method = GET,value = "/contactById/{id}")
    public Contact findProductById(@PathVariable int id) {
        return contactService.getContactById(id);
    }

    @RequestMapping(method = GET,value = "/contactByName/{name}")
    public Contact findProductByName(@PathVariable String name) {
        return contactService.getContactByName(name);
    }

    @RequestMapping(method = PUT,value = "/updateContact")
    public Contact updateProduct(@RequestBody Contact contact) {
        return contactService.updateContact(contact);
    }

    @RequestMapping(method = DELETE,value = "/deleteContact/{id}")
    public String deleteProduct(@PathVariable int id) {
        return contactService.deleteContact(id);
    }
}
