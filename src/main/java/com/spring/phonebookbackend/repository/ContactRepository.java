package com.spring.phonebookbackend.repository;

import com.spring.phonebookbackend.entity.Contact;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactRepository extends JpaRepository<Contact,Integer> {

    Contact findByName(String name);
}
