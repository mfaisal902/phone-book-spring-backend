package com.spring.phonebookbackend.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity
public class Contact {

    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String email;
    private String phoneNumber;

}
