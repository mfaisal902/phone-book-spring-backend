package com.spring.phonebookbackend.service;

import com.spring.phonebookbackend.entity.Contact;
import com.spring.phonebookbackend.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContactService {

    @Autowired
    private ContactRepository repository;


    public Contact saveContact(Contact contact) {
        return repository.save(contact);
    }

    public List<Contact> saveContacts(List<Contact> contact) {
        return repository.saveAll(contact);
    }

    public List<Contact> getContacts() {
        return repository.findAll();
    }

    public Contact getContactById(int id) {
        return repository.findById(id).orElse(null);
    }

    public Contact getContactByName(String name) {
        return repository.findByName(name);
    }

    public String deleteContact(int id) {
        repository.deleteById(id);
        return "Contact Removed having Id : " + id;
    }

    public Contact updateContact(Contact contact) {

        Contact existingContact = repository.findById(contact.getId()).orElse(null);
        if(existingContact != null){
            existingContact.setName(contact.getName());
            existingContact.setEmail(contact.getEmail());
            existingContact.setPhoneNumber(contact.getPhoneNumber());
            return repository.save(existingContact);
        }

        return null;
    }
}
