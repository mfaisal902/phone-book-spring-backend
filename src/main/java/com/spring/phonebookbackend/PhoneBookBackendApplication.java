package com.spring.phonebookbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhoneBookBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(PhoneBookBackendApplication.class, args);
    }

}
