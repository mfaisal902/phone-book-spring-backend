package com.spring.phonebookbackend;

import com.spring.phonebookbackend.entity.Contact;
import com.spring.phonebookbackend.service.ContactService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PhoneBookBackendApplicationTests {

    @Autowired
    private ContactService contactService;

    @Test
    void saveContact(){
        Contact contact = new Contact(9,"Hammad Mehmood","hammad@gmail.com","+923085173886");
        Contact getContact = contactService.saveContact(contact);
        assertNotNull(contactService.getContactById(getContact.getId()));
    }

    @Test
    void deleteContact(){
        Contact contact = new Contact(1,"Vipra","Vipra@gmail.com","+40750866075");

        Contact getContact = contactService.saveContact(contact);

        contactService.deleteContact(getContact.getId());

        assertNull(contactService.getContactById(getContact.getId()));
    }

    @Test
    void updateContact(){
        Contact contact = new Contact(1,"Vipra","Vipra@gmail.com","+40750866075");

        Contact getContact = contactService.saveContact(contact);

        getContact.setEmail("mfaisal902@yahoo.com");
        getContact.setPhoneNumber("+923085173886");
        getContact.setName("Updated Name");

        contactService.updateContact(getContact);

        Contact updatedContact = contactService.getContactById(getContact.getId());

        assertNotEquals(contact.getName(),updatedContact.getName());
        assertNotEquals(contact.getEmail(),updatedContact.getEmail());
        assertNotEquals(contact.getPhoneNumber(),updatedContact.getPhoneNumber());
    }


    @Test
    void getAllContacts(){
        List<Contact> list = contactService.getContacts();
        assertThat(list.size()).isGreaterThan(0);
    }

}
