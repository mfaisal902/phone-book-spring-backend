# Getting Started

### Reference Documentation

For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.7.0/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.7.0/maven-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.7.0/reference/htmlsingle/#web)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.7.0/reference/htmlsingle/#data.sql.jpa-and-spring-data)

### Guides

The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Accessing data with MySQL](https://spring.io/guides/gs/accessing-data-mysql/)

## Installation 
The Following are the required softwares to install
* Download, Install and Configure mysql server + mysql Workbench from the below mentioned link https://dev.mysql.com/downloads/installer/
* After Installation open MYSQL Workbench and create database and use it by using following query + command. 
  * create database phone_book;
  * use phone_book;
* Now you have to install IntelliJ IDEA and import this project into it and follow below mentioned steps for connecting this app with the MYSQL Database.
  * go to resource folder and open properties file
  * write your username and password.
* clone this repo into your intelliJ IDEA and simply run it.
* Also you can run the test cases as well.
